package com.example.weatherforecast.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;

import com.example.weatherforecast.R;

public class GraphActivity extends AppCompatActivity {


    ActionBar actionBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);

        //set graph toolbar
        Toolbar toolbar = findViewById(R.id.graph_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Graphs");
    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent intent= new Intent(GraphActivity.this,MainActivity.class);
        startActivity(intent);
        return super.onSupportNavigateUp();
    }
}
