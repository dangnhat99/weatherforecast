package com.example.weatherforecast.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.app.LauncherActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.weatherforecast.R;
import com.example.weatherforecast.adapter.ViewPagerAdapter;
import com.example.weatherforecast.adapter.WeatherAdapter;
import com.example.weatherforecast.constant.Constants;
import com.example.weatherforecast.fragment.DanhSachFragment;
import com.example.weatherforecast.model.Weather;
import com.example.weatherforecast.task.ParseResult;
import com.example.weatherforecast.task.TaskOutput;
import com.example.weatherforecast.task.WeatherAsynTask;
import com.example.weatherforecast.utils.Format;
import com.example.weatherforecast.utils.UnitConvertor;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import com.example.weatherforecast.utils.DayTimeUtils;

public class MainActivity extends AppCompatActivity {

    private static Map<String, Integer> speedUnits = new HashMap<>(3);
    private static Map<String, Integer> pressUnits = new HashMap<>(3);
    private static boolean mappingsInitialised = false;

    TextView todayTemp,
            todayDescrip,
            todayWind,
            todayPressure,
            todayHumid,
            todaySunrise,
            todaySunset,
            todayUV,
            todayIcon,
            lastUpdate;

    Toolbar toolbar;
    ViewPager viewPager;
    TabLayout tabLayout;
    SwipeRefreshLayout swipeRefreshLayout;
    AppBarLayout appBarLayout;
    ProgressDialog progressDialog;
    Format format;

    Weather todayWeather = new Weather();
    List<Weather> todayList= new ArrayList<>();
    List<Weather> tomorrowList= new ArrayList<>();
    List<Weather> laterList = new ArrayList<>();

    @Override
    protected void onStart() {
        super.onStart();
        updateWeatherList();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       initView();


        initMappings();//hashmap for unit

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //update code here
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            //allow pull to refresh when scrolling to top
            public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
                swipeRefreshLayout.setEnabled(i == 0);
            }
        });
        new TodayWeatherTask(progressDialog,this,this).execute();
        new WeatherListTask(progressDialog,this,this).execute();
    }

    public void initView() {
        //today TextView
        todayTemp = findViewById(R.id.todayTemp);
        todayDescrip = findViewById(R.id.todayDescrib);
        todayWind = findViewById(R.id.todayWind);
        todayPressure = findViewById(R.id.todayPressure);
        todayHumid = findViewById(R.id.todayHumidity);
        todaySunrise = findViewById(R.id.todaySunrise);
        todaySunset = findViewById(R.id.todaySunset);
        todayUV = findViewById(R.id.todayUv);
        todayIcon = findViewById(R.id.todayIcon);
        lastUpdate = findViewById(R.id.lastUpdate);
        //layout
        swipeRefreshLayout = findViewById(R.id.swipeLayoutRefresh);
        appBarLayout = findViewById(R.id.appBarLayout);
        tabLayout = findViewById(R.id.tabLayout);
        //others
        viewPager = findViewById(R.id.viewPager);
        progressDialog = new ProgressDialog(this);
        format = new Format(this);
        //load Toolbar
        toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    public static void initMappings() {
        if (mappingsInitialised)
            return;
        mappingsInitialised = true;
        speedUnits.put("m/s", R.string.speed_unit_mps);
        speedUnits.put("kph", R.string.speed_unit_kph);
        speedUnits.put("mph", R.string.speed_unit_mph);
        speedUnits.put("kn", R.string.speed_unit_kn);

        pressUnits.put("hPa", R.string.pressure_unit_hpa);
        pressUnits.put("kPa", R.string.pressure_unit_kpa);
        pressUnits.put("mm Hg", R.string.pressure_unit_mmhg);
    }

    public WeatherAdapter getAdapter(int id){
        WeatherAdapter weatherAdapter;
        if (id==0) {
            weatherAdapter = new WeatherAdapter(todayList,this);
        }
        if (id==1){
            weatherAdapter = new WeatherAdapter(tomorrowList,this);
        }
        else {
            weatherAdapter = new WeatherAdapter(laterList,this);
        }
        return weatherAdapter;
    }

    private void updateWeatherList(){
        ViewPagerAdapter viewPagerAdapter= new ViewPagerAdapter(getSupportFragmentManager());
        //set ToDay List
        Bundle todayBundle= new Bundle();
        todayBundle.putInt(Constants.day,Constants.TODAY_ID);
        DanhSachFragment todayDanhSachFragment = new DanhSachFragment();
        todayDanhSachFragment.setArguments(todayBundle);
        viewPagerAdapter.addNewFragment(todayDanhSachFragment,Constants.today);
        //Tomorrow List
        Bundle tomorrowBundle= new Bundle();
        tomorrowBundle.putInt(Constants.day,Constants.TODAY_ID);
        DanhSachFragment tomorrowDanhSachFragment = new DanhSachFragment();
        tomorrowDanhSachFragment.setArguments(tomorrowBundle);
        viewPagerAdapter.addNewFragment(tomorrowDanhSachFragment,Constants.tomorrow);
        //Later List
        Bundle laterBundle= new Bundle();
        laterBundle.putInt(Constants.day,Constants.TODAY_ID);
        DanhSachFragment laterDanhSachFragment = new DanhSachFragment();
        laterDanhSachFragment.setArguments(laterBundle);
        viewPagerAdapter.addNewFragment(laterDanhSachFragment,Constants.later);

        //set up viewPager
        viewPagerAdapter.notifyDataSetChanged();
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(viewPager.getCurrentItem(),false);
    }

    private void preLoadWeather() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        String lastToday=sp.getString("lastToday","");
        if (!lastToday.isEmpty()) {
            new TodayWeatherTask(progressDialog,this,this).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,"cachedResponse",lastToday);
        }
        String lastLongterm = sp.getString("lastLongterm","");
        if (!lastLongterm.isEmpty()) {
        }
    }

    @SuppressLint("SetTextI18n")
    private void updateTodayWeatherUI() {
        try {
            if (todayWeather.getCountry().isEmpty()) {
                preLoadWeather();
                return;
            }
        } catch (Exception e) {
            preLoadWeather();
            return;
        }
        String city = todayWeather.getCity();
        String country = todayWeather.getCountry();
        DateFormat dateFormat= android.text.format.DateFormat.getTimeFormat(getApplicationContext());
        getSupportActionBar().setTitle(city+ (country.isEmpty()? "":","+country));

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);

        //temp
        Float temp= UnitConvertor.convertTemp(Float.parseFloat(todayWeather.getTemperature()),sharedPreferences);
        if (sharedPreferences.getBoolean("temperatureInteger",false)) {
           temp =(float) Math.round(temp);
        }

        //rain
        double rain = Double.parseDouble(todayWeather.getRain());
        String rainString= UnitConvertor.getRainString(rain,sharedPreferences);

        //Wind
        double wind;
        try {
            wind = Double.parseDouble(todayWeather.getWind());
        } catch (NumberFormatException e) {
            wind=0;
        }
        wind = UnitConvertor.convertWind(wind,sharedPreferences);

        //pressure
        double pressure = UnitConvertor.convertPressure(Float.parseFloat(todayWeather.getPressure()),sharedPreferences);

        //set text for Today View
        todayTemp.setText(new DecimalFormat("0.#").format(temp)+" "+sharedPreferences.getString("unit","°C"));

        todayDescrip.setText(todayWeather.getDescribtion().substring(0,1).toUpperCase()
                + todayWeather.getDescribtion().substring(1)+rainString);

        if (sharedPreferences.getString("speedUnit","m/s").equals("bft")) {
            todayWind.setText(getString(R.string.wind) +": "+ new DecimalFormat("0.0").format(wind)
                    + " " + UnitConvertor.getBeaufortName((int)wind,this)
            + (todayWeather.isWindDirectionAvailable()?" "+getWindDirectionString(sharedPreferences,this,todayWeather):""));
        }
        else {
            todayWind.setText(getString(R.string.wind) +": "+ new DecimalFormat("0.0").format(wind)
                    + " " + localize(sharedPreferences,"speedUnit","m/s")
                    + (todayWeather.isWindDirectionAvailable()?" "+getWindDirectionString(sharedPreferences,this,todayWeather):""));
        }

        todayPressure.setText(getString(R.string.pressure)+ ": "+ new DecimalFormat("0.0").format(pressure)+" "
        +localize(sharedPreferences,"pressureUnit","hPa"));

        todayHumid.setText(getString(R.string.humidity)+": "+todayWeather.getHumidity()+"%");
        todaySunrise.setText(getString(R.string.sunrise)+": "+dateFormat.format(todayWeather.getSunrise()));
        todaySunset.setText(getString(R.string.sunset)+": "+dateFormat.format(todayWeather.getSunset()));
        todayIcon.setText(todayWeather.getIcon());

        long time = sharedPreferences.getLong("lastUpdate",0);
        Date lastUpdateDate= new Date(time);
        String formatedTime = getString(R.string.last_update, android.text.format.DateFormat.getTimeFormat(this).format(lastUpdateDate));
        lastUpdate.setText(formatedTime);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        DoWorkInItemSelected(item);
        return super.onOptionsItemSelected(item);
    }

    public static long syncCurrentTime(SharedPreferences sharedPreferences){
        Calendar present = Calendar.getInstance();
        sharedPreferences.edit().putLong("lastUpdate", present.getTimeInMillis()).commit();
        return present.getTimeInMillis();
    }

    private ParseResult parseListJson (String result)  {
        try {
            JSONObject listObj= new JSONObject(result);

            String code= listObj.optString("cod");
            if (code.equals("404")) {
                if (todayList== null) {
                    todayList = new ArrayList<>();
                    tomorrowList = new ArrayList<>();
                    laterList = new ArrayList<>();
                }
                return ParseResult.CITY_NOT_FOUND;
            }

            todayList = new ArrayList<>();
            tomorrowList = new ArrayList<>();
            laterList = new ArrayList<>();

            JSONArray list = listObj.getJSONArray("list");
            for (int i=0;i<list.length();i++) {
                Weather itemWeather = new Weather();

                JSONObject listItem = list.getJSONObject(i);
                JSONObject main = listItem.getJSONObject("main");
                JSONArray weather = listItem.getJSONArray("weather");

                itemWeather.setDate(listItem.getString("dt"));
                itemWeather.setTemperature(main.getString("temp"));
                itemWeather.setPressure(main.getString("pressure"));
                itemWeather.setHumidity(main.getString("humidity"));
                itemWeather.setDescribtion(weather.getJSONObject(0).getString("description"));
                itemWeather.setId(weather.getJSONObject(0).getString("id"));

                JSONObject windObj = listItem.getJSONObject("wind");
                if (windObj!= null) {
                    itemWeather.setWind(windObj.getString("speed"));
                    itemWeather.setWindDirectionDec(windObj.getDouble("deg"));
                }

                JSONObject rainObj = listItem.optJSONObject("rain");
                String rain="";
                if (rainObj != null) {
                    rain = getRainString(rainObj);
                }
                else {
                    JSONObject snowObj= listItem.optJSONObject("snow");
                    if (snowObj!= null) {
                        rain= getRainString(snowObj);
                    }
                    else {
                        rain="0";
                    }
                }
                itemWeather.setRain(rain);

                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(Long.parseLong(listItem.getString("dt")+"000"));
                itemWeather.setIcon(format.setIcon(Integer.parseInt(itemWeather.getId()),DayTimeUtils.isDayTime(itemWeather,calendar)));

                //set time
                Calendar today = Calendar.getInstance();
                today.set(Calendar.HOUR_OF_DAY,0);
                today.set(Calendar.MINUTE,0);
                today.set(Calendar.SECOND,0);
                today.set(Calendar.MILLISECOND,0);

                Calendar tomorrow = (Calendar) today.clone();
                tomorrow.add(Calendar.DAY_OF_YEAR,1);

                Calendar later = (Calendar) today.clone();
                later.add(Calendar.DAY_OF_YEAR,2);

                if (calendar.before(tomorrow)) {
                    todayList.add(itemWeather);
                }
                else if (calendar.before(later)) {
                    tomorrowList.add(itemWeather);
                }
                else {
                    laterList.add(itemWeather);
                }
            }
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
            editor.putString("lastLongterm",result).commit();

        }
        catch (JSONException e) {
            e.printStackTrace();
            return ParseResult.JSON_EXCEPTION;
        }
        return ParseResult.PARSED;
    }

    private ParseResult parseTodayJson (String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            String code = jsonObject.optString("cod");
            if (code.equals("404")) {
                return ParseResult.CITY_NOT_FOUND;
            }
            //get city, country, sunrise, sunset
            String city = jsonObject.getString("name");
            String country ="";
            JSONObject countryObj= jsonObject.optJSONObject("sys");
            if (countryObj!= null) {
                country = countryObj.getString("country");
                todayWeather.setSunset(countryObj.getString("sunset"));
                todayWeather.setSunrise(countryObj.getString("sunrise"));
            }
            todayWeather.setCountry(country);
            todayWeather.setCity(city);

            //get lat & lon
            JSONObject coordinates = jsonObject.getJSONObject("coord");
            if (coordinates!= null) {
                todayWeather.setLat(coordinates.getDouble("lat"));
                todayWeather.setLon(coordinates.getDouble("lon"));
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
                preferences.edit().putFloat("latitude",(float) todayWeather.getLat()).putFloat("longitude",(float)todayWeather.getLon());
            }

            JSONObject main = jsonObject.getJSONObject("main");
            //get temp and descrip
            todayWeather.setTemperature(main.getString("temp"));
            todayWeather.setDescribtion(jsonObject.getJSONArray("weather").getJSONObject(0).getString("description"));

            //wind
            JSONObject windObj = jsonObject.getJSONObject("wind");
            todayWeather.setWind(windObj.getString("speed"));
            if (windObj.has("deg")) {
                todayWeather.setWindDirectionDec(windObj.getDouble("deg"));
            }
            else {
                todayWeather.setWindDirectionDec(null);
            }

            //presure,humidity
            todayWeather.setPressure(main.getString("pressure"));
            todayWeather.setHumidity(main.getString("humidity"));

            //rain
            JSONObject rainObj = jsonObject.optJSONObject("rain");
            String rain;
            if (rainObj !=null) {
                rain = getRainString(rainObj);
            }
            else {
                JSONObject snowObj = jsonObject.optJSONObject("snow");
                if (snowObj != null) {
                    rain= getRainString(snowObj);
                }
                 else {
                     rain ="0";
                }
            }
            todayWeather.setRain(rain);

            String idString = jsonObject.getJSONArray("weather").getJSONObject(0).getString("id");
            todayWeather.setId(idString);
            todayWeather.setIcon(format.setIcon(Integer.parseInt(idString),DayTimeUtils.isDayTime(todayWeather,Calendar.getInstance())));

            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit();
            //save data json TodayWeather gan nhat -->l
            editor.putString("lastToday",result).commit();

        } catch (JSONException e) {
            e.printStackTrace();
            return ParseResult.JSON_EXCEPTION;
        }

        return ParseResult.PARSED;
    }


    public static String getRainString(JSONObject obj) {
        String rain ="0";
        if (obj!=null) {
            rain = obj.optString("3h","fail");
            if (rain.equals("fail")) {
                rain = obj.optString("1h","0");
            }
        }
        return rain;
    }

    public static String getWindDirectionString(SharedPreferences sp, Context context, Weather weather) {
        try {
            if (Double.parseDouble(weather.getWind()) != 0) {
                String pref = sp.getString("windDirectionFormat", "arrow");
                if ("arrow".equals(pref)) {
                    return weather.getWindDirection(8).getArrow(context);
                } else if ("abbr".equals(pref)) {
                    return weather.getWindDirection().getlocalizedString(context);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    private String localize(SharedPreferences sp, String preferenceKey, String defaultValueKey) {
        return localize(sp, this, preferenceKey, defaultValueKey);
    }

    public static String localize(SharedPreferences sp, Context context, String preferenceKey, String defaultValueKey) {
        String preferenceValue = sp.getString(preferenceKey, defaultValueKey);
        String result = preferenceValue;
        if ("speedUnit".equals(preferenceKey)) {
            if (speedUnits.containsKey(preferenceValue)) {
                result = context.getString(speedUnits.get(preferenceValue));
            }
        } else if ("pressureUnit".equals(preferenceKey)) {
            if (pressUnits.containsKey(preferenceValue)) {
                result = context.getString(pressUnits.get(preferenceValue));
            }
        }
        return result;
    }

    private void DoWorkInItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.MainMenuWeatherMap:
                //code here
                break;
            case R.id.MainMenuGraphs:
                //code here
                Intent intent = new Intent(MainActivity.this,GraphActivity.class);
                startActivity(intent);
                break;
            case R.id.MainMenuLocation:
                //code here
                break;
            case R.id.MainMenuSetting:
                //code
                break;
            case R.id.MainMenuAbout:
                //code
                break;
            case R.id.SearchMenu:
                //code
                break;
            case R.id.RefreshMenu:
                //code
                break;
        }
    }

    class TodayWeatherTask extends WeatherAsynTask {

        public TodayWeatherTask(ProgressDialog progressDialog, Context context, MainActivity activity) {
            super(progressDialog, context, activity);
        }

        @Override
        protected void onPreExecute() {
            loading =0;
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(TaskOutput taskOutput) {
            super.onPostExecute(taskOutput);
        }

        @Override
        protected String getAPIname() {
            return "weather";
        }

        @Override
        protected ParseResult ParseRespond(String respond) {
            return parseTodayJson(respond);
        }

        @Override
        protected void UpdateUI() {
            updateTodayWeatherUI();
        }
    }

    class WeatherListTask extends WeatherAsynTask {

        public WeatherListTask(ProgressDialog progressDialog, Context context, MainActivity activity) {
            super(progressDialog, context, activity);
        }

        @Override
        protected void UpdateUI() {
            updateWeatherList();
        }

        @Override
        protected String getAPIname() {
            return "forecast";
        }

        @Override
        protected ParseResult ParseRespond(String respond) {
            return parseListJson(respond) ;
        }
    }
}
