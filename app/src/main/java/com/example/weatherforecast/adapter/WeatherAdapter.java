package com.example.weatherforecast.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weatherforecast.R;
import com.example.weatherforecast.model.Weather;
import com.example.weatherforecast.utils.UnitConvertor;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.ViewHolder> {
    List<Weather> weatherList;
    Context context;

    public WeatherAdapter(List<Weather> weatherList, Context context) {
        this.weatherList = weatherList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater= LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.weather_row,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Weather itemWeather = weatherList.get(position);

        SharedPreferences sp= PreferenceManager.getDefaultSharedPreferences(context);

        //temp
        float temp= UnitConvertor.convertTemp(Float.parseFloat(itemWeather.getTemperature()),sp);
        if (sp.getBoolean("temperaturenInteger",false)) {
            temp = Math.round(temp);
        }

        //Rain
        double rain= Double.parseDouble(itemWeather.getRain());
        String rainString = UnitConvertor.getRainString(rain,sp);

        //Wind
        double wind;
        try {
            wind = Double.parseDouble(itemWeather.getWind());
        } catch (Exception e) {
            e.printStackTrace();
            wind=0;
        }
        wind = UnitConvertor.convertWind(wind,sp);

        //Pressure
        double pressure = UnitConvertor.convertPressure((float) Double.parseDouble(itemWeather.getPressure()),sp);

        TimeZone timeZone= TimeZone.getDefault();
        String dDateFormat = context.getResources().getStringArray(R.array.dateFormatsValues)[0];
        String dateFormat = sp.getString("dateFormat",dDa)
        holder.itemDate.setText(itemWeather.getDate().toString());
        holder.itemTemp.setText(itemWeather.getTemperature());
        holder.itemDescrip.setText(itemWeather.getDescribtion());
        holder.itemWind.setText(itemWeather.getWind());
        holder.itemPressure.setText(itemWeather.getPressure());
        holder.itemHumid.setText(itemWeather.getHumidity());
        holder.itemIcon.setText(itemWeather.getIcon());

    }

    @Override
    public int getItemCount() {
        return weatherList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemDate;
        TextView itemTemp;
        TextView itemDescrip;
        TextView itemWind;
        TextView itemPressure;
        TextView itemHumid;
        TextView itemIcon;
        View lineView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemDate = itemView.findViewById(R.id.itemDate);
            itemTemp = itemView.findViewById(R.id.itemTemp);
            itemDescrip = itemView.findViewById(R.id.itemDescrib);
            itemWind = itemView.findViewById(R.id.itemWind);
            itemPressure =itemView.findViewById(R.id.itemPressure);
            itemHumid = itemView.findViewById(R.id.itemHumid);
            itemIcon = itemView.findViewById(R.id.itemIcon);
            lineView = itemView.findViewById(R.id.divider);
        }
    }
}
