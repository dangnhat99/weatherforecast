package com.example.weatherforecast.constant;

public class Constants {
    public static String day = "day";
    public static String today= "Today";
    public static int TODAY_ID=0;
    public static int TOMORROW_ID=1;
    public static int LATER_ID=2;
    public static String tomorrow = "Tomorrow";
    public static String later = "Later";

    public static final String DEFAULT_CITY_ID = "1580240";

}
