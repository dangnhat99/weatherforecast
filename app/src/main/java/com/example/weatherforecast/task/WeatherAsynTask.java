package com.example.weatherforecast.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import androidx.preference.PreferenceManager;

import com.example.weatherforecast.R;
import com.example.weatherforecast.activity.MainActivity;
import com.example.weatherforecast.constant.Constants;
import com.example.weatherforecast.utils.Language;
import com.google.android.material.snackbar.Snackbar;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public abstract class WeatherAsynTask extends AsyncTask<String, String, TaskOutput> {
   ProgressDialog progressDialog;
   Context context;
   MainActivity activity;
   public int loading = 0;

    public WeatherAsynTask(ProgressDialog progressDialog, Context context, MainActivity activity) {
        this.progressDialog = progressDialog;
        this.context = context;
        this.activity = activity;
    }

    @Override//Init progressDialog
    protected void onPreExecute() {
        Uploading();
        if (!progressDialog.isShowing()) {
            progressDialog.setMessage("Downloading...");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }
    }

    @Override
    protected TaskOutput doInBackground(String... strings) {
        TaskOutput taskOutput = new TaskOutput();

        String response = "";
        String[] regParams = new String[]{};

        if (strings != null && strings.length > 0) {
           // String zeroParam = strings[0];
            if ("cachedResponse".equals(strings[0])) {
                response = strings[1];
                taskOutput.taskResult = TaskResult.SUCCESS;
            }
            else if ("coords".equals(strings[0])) {
                String lat= strings[1];
                String lon= strings[2];
                regParams = new String[]{"coords",lat,lon};
            }
            else if ("city".equals(strings[0])) {
                regParams= new String[]{"city",strings[1]};
            }
        }

        if (response.isEmpty()) {
            try {
                URL url = URLprovide(regParams);
                Log.d("URL", "doInBackground: "+url.toString());
                HttpURLConnection urlConnection= (HttpURLConnection) url.openConnection();
                //Read success
                if (urlConnection.getResponseCode()==200) {
                    InputStreamReader inputStreamReader= new InputStreamReader(urlConnection.getInputStream());
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line);
                        stringBuilder.append("\n");
                    }
                    response += stringBuilder.toString();
                    Close(bufferedReader);
                    urlConnection.disconnect();
                    taskOutput.taskResult = TaskResult.SUCCESS;

                    //Save the time that success update
                    activity.syncCurrentTime(PreferenceManager.getDefaultSharedPreferences(context));
                }
                //Too many request
                else if (urlConnection.getResponseCode()== 429) {
                    taskOutput.taskResult = TaskResult.TOO_MANY_REQUEST;
                }
                //Bad response
                else {
                    taskOutput.taskResult = TaskResult.BAD_RESPONSE;
                }

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
                taskOutput.taskResult = TaskResult.IO_EXCEPTION;
            }

            //process result
            if (TaskResult.SUCCESS.equals(taskOutput.taskResult)) {
                ParseResult parseResult = ParseRespond(response);
                if (ParseResult.CITY_NOT_FOUND.equals(parseResult)) {
                    //restore previous city;
                }
                taskOutput.parseResult = parseResult;
            }
        }
        return taskOutput;
    }

    @Override
    protected void onPostExecute(TaskOutput taskOutput) {
        if (loading ==1 ){
            progressDialog.dismiss();
            Downloading();
            UpdateUI();
            handleTaskOutput(taskOutput);
        }
    }

    private URL URLprovide (String[] regParams) throws UnsupportedEncodingException, MalformedURLException {
        SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);

        String apiKey= sharedPreferences.getString("API","c5951235693e0448cc204b40e83dd31b");

//

        StringBuilder urlBuilder = new StringBuilder("https://api.openweathermap.org/data/2.5/");
        urlBuilder.append(getAPIname()).append("?");
        if (regParams.length>0) {
            if ("coords".equals(regParams[0])) {
                urlBuilder.append("lat=").append(regParams[1]).append("&lon=").append(regParams[2]);
            }
            else if ("city".equals(regParams[0])){
                urlBuilder.append("q=").append(regParams[1]);
            }
        }
        //default constant
        else {
            String cityId= sharedPreferences.getString("cityId", Constants.DEFAULT_CITY_ID);
            urlBuilder.append("id=").append(URLEncoder.encode(cityId,"UTF-8"));
        }

        urlBuilder.append("&lang=").append(Language.getLanguage());
        urlBuilder.append("&mode=json");
        urlBuilder.append("&appid=").append(apiKey);
        return new URL(urlBuilder.toString());
    }

    private void Close(Closeable close){
       try {
           if (close!= null) {
               close.close();
           }
       }
       catch (IOException e) {
           Log.e("IO exception", "Close: Error closing stream" );
       }
    }

    protected  void handleTaskOutput(TaskOutput output){
        switch (output.taskResult) {
            case SUCCESS: {
                ParseResult parseResult = output.parseResult;
                if (ParseResult.CITY_NOT_FOUND.equals(parseResult)) {
                    Snackbar.make(activity.findViewById(android.R.id.content),"City not found",Snackbar.LENGTH_LONG).show();
                }
                else if(ParseResult.JSON_EXCEPTION.equals(parseResult)) {
                    Snackbar.make(activity.findViewById(android.R.id.content),"Error parsing JSON",Snackbar.LENGTH_LONG).show();
                }
                break;
            }
            case TOO_MANY_REQUEST: {
                Snackbar.make(activity.findViewById(android.R.id.content),"Too many request. Try next time!",Snackbar.LENGTH_LONG).show();
                break;
            }
            case BAD_RESPONSE: {
                Snackbar.make(activity.findViewById(android.R.id.content),"Internet connection failed!",Snackbar.LENGTH_LONG).show();
                break;
            }
            case IO_EXCEPTION: {
                Snackbar.make(activity.findViewById(android.R.id.content),"Connect not available",Snackbar.LENGTH_LONG).show();
                break;
            }
        }
    }

    private void Uploading(){
        loading++;
    }

    void Downloading(){
        loading--;
    }

    protected  void UpdateUI() { }

    protected abstract String getAPIname();
    protected abstract ParseResult ParseRespond(String respond);
}
