package com.example.weatherforecast.utils;

import com.example.weatherforecast.model.Weather;

import java.util.Calendar;
import java.util.Date;

public class DayTimeUtils {
    public static boolean isDayTime (Weather weather, Calendar calendar) {
        Date Sunrise= weather.getSunrise(),
                Sunset = weather.getSunset();
        boolean day;
        if (Sunrise !=null && Sunset!= null) {
            Date currentTime= Calendar.getInstance().getTime();

            day=currentTime.after(Sunrise) && currentTime.before(Sunset);
        }
        else {
            //neu ko doc dc sunrise & sunset thi gan mac dinh day=[7h,18h]
            int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
            day = (hourOfDay>=7 && hourOfDay<18);
        }
        return day;
    }
}
